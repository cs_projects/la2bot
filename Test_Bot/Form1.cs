﻿using Interceptor;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;
using System.Windows.Input;
using Keys = System.Windows.Forms.Keys;
using Key = Interceptor.Keys;
using System.Text;
using System.Diagnostics;

namespace Test_Bot
{
	public partial class Form1 : Form
	{
		public static Hashtable windowInfo = new Hashtable();

		public string imagePath = $"{AppDomain.CurrentDomain.BaseDirectory}img\\";
		public string imageMask = "+*.png";
		public string[] texturesPng;

		[DllImport("User32.dll")]
		public static extern bool RegisterHotKey(IntPtr hWnd, int id, uint fsModifiers, uint vk);

		[DllImport("User32.dll")]
		public static extern bool UnregisterHotKey(IntPtr hWnd, int id);

		[DllImport("kernel32.dll")]
		public static extern Int16 GlobalAddAtom(string name);

		[DllImport("kernel32.dll")]
		public static extern Int16 GlobalDeleteAtom(Int16 nAtom);

		private static Dictionary<Int16, Action> _globalActions = new Dictionary<short, Action>();

		public static Input input;

		public static bool botThreadStop;

		private static Thread botThread;

		public int result = 0;

		public Dictionary<string, Action> actions;


		[DllImport("user32.dll")]
		public static extern bool SetForegroundWindow(IntPtr hWnd);

		public Form1()
		{
			InitializeComponent();
		}


		private void StartEnchant()
		{
			if (windowInfo.Count < 1)
			{
				MessageBox.Show("Выберите окно для заточки!");
				return;
			}

			texturesPng = Directory.GetFiles(imagePath, imageMask);
			if (texturesPng.Count() <= 1)
			{
				MessageBox.Show("Выберите папку с текстурами заточенных итемов в формате PNG!");
				return;
			}

			windowInfo["imagePath"] = imagePath;
			windowInfo["startEnchant"] = nudStart.Value;
			windowInfo["stopEnchant"] = nudStop.Value;

			MouseFilterMode[] filter = { MouseFilterMode.None, MouseFilterMode.All, MouseFilterMode.LeftDown, MouseFilterMode.LeftUp, MouseFilterMode.RightDown, MouseFilterMode.RightUp, MouseFilterMode.MiddleDown, MouseFilterMode.MiddleUp, MouseFilterMode.LeftExtraDown, MouseFilterMode.LeftExtraUp, MouseFilterMode.RightExtraDown, MouseFilterMode.RightExtraUp, MouseFilterMode.MouseWheelVertical, MouseFilterMode.MouseWheelHorizontal, MouseFilterMode.MouseMove };

			input = new Input
			{
				KeyboardFilterMode = KeyboardFilterMode.All,
				MouseFilterMode = filter[(int)numericUpDown1.Value]
			};

			StartBotThread("Enchant1");

			RegisterGlobalHotkey(() => StopEnchant(), Keys.Escape);

			nudStart.Enabled = false;
			nudStop.Enabled = false;
			bStart.Enabled = false;
			numericUpDown1.Enabled = false;

			bFind.Enabled = false;

			return;
		}


		public void StartBotThread(string characterClass)
		{
			input.Load();

			botThreadStop = true;

			botThread = new Thread(new ParameterizedThreadStart(new ParameterizedThreadStart((x) =>
			{
				StartBot(characterClass);
			})))
			{
				Priority = ThreadPriority.Highest
			};

			botThread.Start();
		}


		public void StopBotThread()
		{
			botThreadStop = false;
		}


		private void StartBot(string characterClass)
		{
			actions = new Dictionary<string, Action>();

			Enchant1 enchant1 = new Enchant1();
			RegisterBots(() => result = enchant1.LoadBot(ref botThreadStop), "Enchant1");

			if (actions.ContainsKey(characterClass))
				actions[characterClass]();

			if (result == 2)
			{
				MessageBox.Show("Что-то пошло не так, нажмите клавишу Stop и повторите снова!");
			}
			else if (result == 0)
			{
				MessageBox.Show("Бот успешно остановлен!");
			}
			else if (result == 3)
			{
				SetForegroundWindow(Handle);
				MessageBox.Show("Закончились точки!");
			}
			else if (result == 1)
			{
				MessageBox.Show("Заточка успешно завершена!");
			}

			input.Unload();
			SetForegroundWindow(Handle);
		}


		public void RegisterBots(Action action, string commonKey)
		{
			actions.Add(commonKey, action);
		}


		private void StopEnchant()
		{
			nudStart.Enabled = true;
			nudStop.Enabled = true;
			bStart.Enabled = true;
			bStop.Enabled = true;
			bFind.Enabled = true;
			numericUpDown1.Enabled = true;
			UnregisterHotkeys();
			StopBotThread();
		}


		public bool RegisterGlobalHotkey(Action action, Keys commonKey, params ModifierKeys[] keys)
		{
			uint mod = 0;
			if (keys.Length != 0)
				mod = keys.Cast<uint>().Aggregate((current, modKey) => current | modKey);

			short atom = GlobalAddAtom("OurAmazingApp" + (_globalActions.Count + 1));
			bool status = RegisterHotKey(Handle, atom, mod, (uint)commonKey);

			if (status)
				_globalActions.Add(atom, action);

			return status;
		}


		public void UnregisterHotkeys()
		{
			foreach (var atom in _globalActions.Keys)
			{
				UnregisterHotKey(Handle, atom);
				GlobalDeleteAtom(atom);
			}

			_globalActions = new Dictionary<short, Action>();
		}


		protected override void WndProc(ref Message m)
		{
			if (m.Msg == 0x0312)
			{
				short atom = Int16.Parse(m.WParam.ToString());
				if (_globalActions.ContainsKey(atom))
					_globalActions[atom]();

				return;
			}

			base.WndProc(ref m);
		}


		private void Form1_Load(object sender, EventArgs e)
		{
			if (tabControl1.SelectedIndex == 0)
			{
				Text = $"Выбранная папка с текстурами заточек: {imagePath}";
				Directory.CreateDirectory(imagePath);
			}
			else if (tabControl1.SelectedIndex == 1)
			{
				Text = $"Выбранная папка с текстурами заточек: 2!";
			}
			else
			{
				Text = $"Выбранная папка с текстурами заточек: {imagePath}";
				Directory.CreateDirectory(imagePath);
			}
		}


		private void bFind_Click(object sender, EventArgs e)
		{
			Functions Functions = new Functions();
			windowInfo = Functions.GetWindowInfo();
		}


		private void bStart_Click(object sender, EventArgs e)
		{
			StartEnchant();
		}


		private void bStop_Click(object sender, EventArgs e)
		{
			StopEnchant();
		}


		private void openToolStripMenuItem_Click(object sender, EventArgs e)
		{
			string fileName = openFileDialog1.FileName = "Folder Selection";
			openFileDialog1.InitialDirectory = AppDomain.CurrentDomain.BaseDirectory;
			if (openFileDialog1.ShowDialog() == DialogResult.OK)
			{
				string fileName2 = openFileDialog1.FileName;
				openFileDialog1.FileName = fileName2.Replace("\\" + fileName, "\\");
				imagePath = openFileDialog1.FileName;
				Text = $"Выбранная папка с текстурами заточек: {openFileDialog1.FileName}";
			}
		}


		private void tabPage1_Enter(object sender, EventArgs e)
		{
			Text = $"Выбранная папка с текстурами заточек: {imagePath}";
			Directory.CreateDirectory(imagePath);
		}

		private void tabPage2_Enter(object sender, EventArgs e)
		{
			Text = $"Выбранная папка с текстурами заточек: 2!";
		}
	}
}
