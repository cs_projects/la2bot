﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Interceptor;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Test_Bot
{
	public class Bot : Form1
	{

		public Point windowPoint;
		private Size windowSize;
		public Rectangle windowRect;

		public Dictionary<string, Point> match;
		private Dictionary<string, Size> size = new Dictionary<string, Size>()
		{
			{ "Inventory", new Size(394, 358) },
			{ "Enchant", new Size(374, 323) }
		};

		public Rectangle matchItem = new Rectangle();


		private Dictionary<string, string> msgMap = new Dictionary<string, string>();

		public static Image<Gray, Byte> windowImageSource;


		public Bot()
		{
			msgMap["Enchant"] = msgMap["btnStart"] = msgMap["btnClose"] = msgMap["PutItem"] = msgMap["btnSuccess"] = "Не удается найти элементы окна точки, пожалуйста откройте окно или положите точки на F1!";
			msgMap["Window"] = "Не удалось активировать окно!";
			msgMap["WindowCoords"] = "Не удается получить координаты окна!";
			msgMap["Inventory"] = "Не удается найти инвентарь, пожалуйста откройте инвентарь на вкладке \"Снаряжение\"!";
			match = new Dictionary<string, Point>(msgMap.Count);
		}


		public bool SetWindow()
		{
			foreach (var item in windowInfo.Keys)
			{
				if (item.ToString() == "HWND")
				{
					//bool lalala = int.TryParse(item.Value.ToString(), out int temp);
					bool bhWnd = int.TryParse(windowInfo[item].ToString(), out int temp);
					IntPtr hWnd;
					if (bhWnd)
					{
						hWnd = (IntPtr)temp;
						if (SetForegroundWindow(hWnd))
							Thread.Sleep(500);
						else
						{
							SetForegroundWindow(Handle);
							MessageBox.Show(msgMap["Window"]);
							return false;
						}
						
					}
				}
			}
			return true;
		}


		public bool GetWindowCoords()
		{

			if (int.TryParse(windowInfo["width"].ToString(), out int width) && int.TryParse(windowInfo["height"].ToString(), out int height) && int.TryParse(windowInfo["x"].ToString(), out int X) && int.TryParse(windowInfo["y"].ToString(), out int Y))
			{
				if (X < 0)
					X += 8;
				if (Y < 0)
					Y += 8;

				windowPoint = new Point(X, Y);
				windowSize = new Size(width, height);
				windowRect = new Rectangle(windowPoint, windowSize);

				match["Window"] = new Point(X, Y);
				size["Window"] = new Size(width, height);

				match["buff"] = new Point(X, Y + 23);
				size["buff"] = new Size(width / 2, 130);
				
				match["luc"] = new Point(X + 20, Y + 935);
				size["luc"] = new Size(340, 40);
			}
			else
			{
				SetForegroundWindow(Handle);
				MessageBox.Show(msgMap["WindowCoords"]);
				return false;
			}

			return true;
		}


		public bool StartFinder()
		{
			if (!SetWindow())
				return false;
			else if (!GetWindowCoords())
				return false;

			string[] names = new string[5] { "Inventory", "Enchant", "btnStart", "btnClose", "PutItem" };

			if (!AddMatch(names, GetElement("Window")))
				return false;
			return true;
		}

		public bool AddMatch(string[] names, Image<Gray, Byte> source)
		{
			Dictionary<string, Image<Gray, float>> result = new Dictionary<string, Image<Gray, float>>();
			Dictionary<string, Size> points = new Dictionary<string, Size>();
			foreach (string name in names)
			{
				Image<Gray, Byte> template = new Image<Gray, Byte>(windowInfo["imagePath"] + name + ".png");
				result[name] = source.MatchTemplate(template, TemplateMatchingType.CcoeffNormed);
				points[name] = new Size(template.Size.Width, template.Size.Height);
			}

			foreach (KeyValuePair<string, Image<Gray, float>> item in result)
			{
				double[] minValues, maxValues;
				Point[] minLocations, maxLocations;

				item.Value.MinMax(out minValues, out maxValues, out minLocations, out maxLocations);

				if (maxValues[0] > 0.99)
				{
					if (item.Key == "Inventory")
					{
						maxLocations[0].X += windowPoint.X - 59;
						maxLocations[0].Y += windowPoint.Y + 31;
					}
					else if (item.Key.StartsWith("+"))
					{
						maxLocations[0].X += match["Inventory"].X + (points[item.Key].Width / 2);
						maxLocations[0].Y += match["Inventory"].Y + (points[item.Key].Height / 2);
					}
					else if (item.Key == "btnStart" || item.Key == "btnClose" || item.Key == "PutItem" || item.Key == "btnSuccess" || item.Key == "btnLuc" || item.Key == "btnLucStart")
					{
						maxLocations[0].X += points[item.Key].Width / 2;
						maxLocations[0].Y += points[item.Key].Height / 2;
					}
					match[item.Key] = maxLocations[0];
				}
				else
				{
					if (msgMap.ContainsKey(item.Key))
					{
						SetForegroundWindow(Handle);
						MessageBox.Show(msgMap[item.Key]);
						return false;
					}
					else
					{
						return false;
					}
				}
			}

			return true;
		}


		public Image<Gray, Byte> GetElement(string name)
		{
			Bitmap bitmap = new Bitmap(size[name].Width, size[name].Height);
			Point point = new Point(match[name].X, match[name].Y);
			using (Graphics graphics = Graphics.FromImage(bitmap))
			{
				graphics.CopyFromScreen(point, Point.Empty, bitmap.Size);
			}

			return bitmap.ToImage<Gray, Byte>();
		}


		public Image<Gray, Byte> GetElement(string name, bool save = false)
		{
			if (save)
			{
				if (windowImageSource == null)
					windowImageSource = GetElement(name);

				return windowImageSource;
			}
			else
				return GetElement(name);
		}


	}
}
