﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Management;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace Test_Bot
{
	public class Functions : Form
	{
		[DllImport("user32.dll")]
		private static extern IntPtr FindWindow(string lpClassName, string lpWindowName);


		[DllImport("user32.dll")]
		private static extern uint GetWindowThreadProcessId(IntPtr hWnd, out uint lpdwProcessId);

		[DllImport("user32.dll", CharSet = CharSet.Unicode, SetLastError = true)]
		private static extern int GetWindowText(IntPtr hWnd, [Out] StringBuilder lpString, int nMaxCount);

		[DllImport("user32.dll", CharSet = CharSet.Unicode, SetLastError = true)]
		private static extern int GetWindowTextLength(IntPtr hWnd);

		[DllImport("user32.dll")]
		private static extern bool EnumWindows(EnumWindowsProc enumProc, Hashtable lParam);

		// Delegate to filter which windows to include 
		public delegate Hashtable EnumWindowsProc(IntPtr hWnd, Hashtable lParam);

		[DllImport("user32.dll")]
		[return: MarshalAs(UnmanagedType.Bool)]
		private static extern bool GetWindowRect(IntPtr hWnd, out RECT lpRect);

		[StructLayout(LayoutKind.Sequential)]
		private struct RECT
		{
			public int Left;        // x position of upper-left corner
			public int Top;         // y position of upper-left corner
			public int Right;       // x position of lower-right corner
			public int Bottom;      // y position of lower-right corner
		}

		private Rectangle rect = new Rectangle();

		private const string CONTAIN_WORD = "account=";
		private Process[] localByName;
		public static IDictionary<string, string> cmdLines;
		private const string WINDOW_SUBSTRING = "Lineage II";
		private const string PROCESS_NAME = "L2.exe";

		public Hashtable windowInfo = new Hashtable();


		public Hashtable GetWindowInfo()
		{
			windowInfo.Add("pid", GetPid().ToString());
			EnumWindows(SetWindowsCoordinates, windowInfo);
			return windowInfo;
		}


		private Hashtable SetWindowsCoordinates(IntPtr hWnd, Hashtable lParam)
		{
			GetWindowThreadProcessId(hWnd, out uint processid);

			if (lParam["pid"].ToString() == processid.ToString())
			{
				int length = GetWindowTextLength(hWnd);

				if (length < 1)
				{
					int errorCode = Marshal.GetLastWin32Error();
					throw new Win32Exception(errorCode, "Fail to get text length");
				}

				StringBuilder lpString = new StringBuilder(length + 1);
				GetWindowText(hWnd, lpString, lpString.Capacity);

				if (lpString.ToString().StartsWith(WINDOW_SUBSTRING))
				{
					if (!GetWindowRect(hWnd, out RECT rct))
						MessageBox.Show("ERROR");

					rect.X = rct.Left;
					rect.Y = rct.Top;

					rect.Width = rct.Right - rct.Left;
					rect.Height = rct.Bottom - rct.Top;
					lParam.Add("x", rect.X);
					lParam.Add("y", rect.Y);
					lParam.Add("width", rect.Width);
					lParam.Add("height", rect.Height);
					lParam.Add("HWND", hWnd);
				}
			}

			return lParam;
		}


		private int GetPid()
		{
			localByName = Process.GetProcessesByName(Path.GetFileNameWithoutExtension(PROCESS_NAME));
			cmdLines = new Dictionary<string, string>(localByName.Length);

			string commandLine;

			foreach (Process proc in localByName)
			{
				if (!cmdLines.ContainsKey(proc.Id.ToString()))
				{
					commandLine = GetCommandLine(proc.Id);
					string[] words = commandLine.Split(' ');

					foreach (var word in words)
					{
						if (word.StartsWith(CONTAIN_WORD))
							cmdLines.Add(proc.Id.ToString(), word);
					}
				}
			}

			Form2 Form2 = new Form2();
			Form2.ShowDialog();
			int pid = Form2.pid;

			return pid;
		}


		private string GetCommandLine(int id)
		{
			using (ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT CommandLine FROM Win32_Process WHERE ProcessId = " + id))
			using (ManagementObjectCollection objects = searcher.Get())
			{
				return objects.Cast<ManagementBaseObject>().SingleOrDefault()?["CommandLine"]?.ToString();
			}
		}

	}
}
