﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Test_Bot
{
	public partial class Form2 : Functions
	{
		public int pid;


		public Form2()
		{
			InitializeComponent();
		}


		private void Form2_Load(object sender, EventArgs e)
		{
			int top = 10;

			Padding pad = new Padding();
			pad.Bottom = 50;

			foreach (KeyValuePair<string, string> kvp in cmdLines)
			{
				Button button = new Button();

				button.Width = 180;
				button.Height = 50;

				Padding margin = button.Margin;
				margin.Bottom = 5;
				button.Margin = margin;

				button.Top = top;
				button.Left = (Width - (button.Width + (button.Margin.Left * 2) + 13)) / 2;

				button.Name = kvp.Key;
				button.Text = $"Выбрать окно:\nPID = {kvp.Key}\n{kvp.Value}";
				button.Click += ButtonOnClick;

				this.Controls.Add(button);
				top += button.Height + 5;
			}
		}


		private void ButtonOnClick(object sender, EventArgs eventArgs)
		{
			if (sender is Button button)
			{
				bool res = int.TryParse(button.Name, out pid);

				if (res)
					Close();
			}
		}

	}
}
