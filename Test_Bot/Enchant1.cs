﻿using Interceptor;
using System;
using System.Collections;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;
using Newtonsoft.Json;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Util;
using Emgu.CV.Structure;
using Keys = Interceptor.Keys;
using System.ComponentModel;
using System.Runtime.InteropServices;

namespace Test_Bot
{
	public sealed class Enchant1 : Bot
	{
		private readonly Random rnd = new Random();
		private readonly string[] buff = { "__Beer1", "__Beer2", "__Transform1" };
		private readonly string buff2 = "__Luc";


		public Enchant1()
		{

		}


		public int LoadBot(ref bool botThreadStop)
		{
			try
			{

				input.KeyPressDelay = 50;
				input.ClickDelay = 50;

				if (!int.TryParse(windowInfo["startEnchant"].ToString(), out int start) & !int.TryParse(windowInfo["stopEnchant"].ToString(), out int end))
				{
					MessageBox.Show("Не удалось получить значения заточки!");
					SetForegroundWindow(Handle);
					return 2;
				}

				if (start >= 14 || start >= end || end > 15 || end < 1)
				{
					MessageBox.Show("Введен неверный диапазон заточки!\n\"Старт заточки\" должен быть не больше 14, не равен \"Конец заточки\" и не больше \"Конец заточки\"\n\"Конец заточки\" должен быть не больше 15, не меньше 1 и не меньше \"Старт заточки\"!");
					SetForegroundWindow(Handle);
					return 2;
				}

				if (!StartFinder())
				{
					SetForegroundWindow(Handle);
					return 2;
				}

				int countLuc = 0;

				for (int currentStep = start; currentStep < end; currentStep++)
				{
					if (!botThreadStop)
					{
						SetForegroundWindow(Handle);
						return 0;
					}

					//while (FindItem(currentStep))
					while (AddMatch(new string[] { "+" + currentStep.ToString() }, GetElement("Inventory")))
					{
						if (!botThreadStop)
						{
							SetForegroundWindow(Handle);
							return 0;
						}

						if (currentStep >= 3)
						{
							foreach (string item in buff)
							{
								//if (!FindBuff(item))
								if (!AddMatch(new string[] { item }, GetElement("buff")))
								{
									if (item == "__Beer1")
									{
										Thread.Sleep(rnd.Next(50, 88));
										input.SendKey(Keys.F9);
										Thread.Sleep(rnd.Next(200, 238));
									}
									else if (item == "__Beer2")
									{
										Thread.Sleep(rnd.Next(50, 88));
										input.SendKey(Keys.F10);
										Thread.Sleep(rnd.Next(200, 238));
									}
									else if (item == "__Transform1")
									{
										Thread.Sleep(rnd.Next(50, 88));
										input.SendKey(Keys.F12);
										Thread.Sleep(rnd.Next(200, 238));
									}
								}
							}
						}

						if (currentStep >= 8)
						{
							if (!botThreadStop)
							{
								SetForegroundWindow(Handle);
								return 0;
							}

							if (countLuc >= 2)
							{
								Thread.Sleep(rnd.Next(70, 138));
								input.SendKey(Keys.Enter);
								Thread.Sleep(rnd.Next(70, 138));
								input.SendText("abc");
								Thread.Sleep(rnd.Next(70, 138));
								input.SendKey(Keys.Enter);
								Thread.Sleep(rnd.Next(70, 138));

								countLuc = 1;
							}
							else
								countLuc += 1;

							while (!AddMatch(new string[] { buff2 }, GetElement("luc")))
							{
								if (!botThreadStop)
								{
									SetForegroundWindow(Handle);
									return 0;
								}
								Thread.Sleep(rnd.Next(100, 138));
								input.SendKey(Keys.F2);
								Thread.Sleep(rnd.Next(100, 138));

								if (!match.ContainsKey("btnLuc"))
								{
									if (!AddMatch(new string[] { "btnLuc" }, GetElement("Window")))
									{
										SetForegroundWindow(Handle);
										return 2;
									}
								}
								if (!botThreadStop)
								{
									SetForegroundWindow(Handle);
									return 0;
								}

								input.MoveMouseTo(match["btnLuc"].X, match["btnLuc"].Y);
								Thread.Sleep(rnd.Next(70, 108));
								input.SendLeftClick();
								Thread.Sleep(rnd.Next(100, 138));
								if (!botThreadStop)
								{
									SetForegroundWindow(Handle);
									return 0;
								}

								if (!match.ContainsKey("btnLucStart"))
								{
									if (!AddMatch(new string[] { "btnLucStart" }, GetElement("Window")))
									{
										SetForegroundWindow(Handle);
										return 2;
									}
								}
								if (!botThreadStop)
								{
									SetForegroundWindow(Handle);
									return 0;
								}

								input.MoveMouseTo(match["btnLucStart"].X, match["btnLucStart"].Y);
								Thread.Sleep(rnd.Next(70, 108));
								input.SendLeftClick();
								Thread.Sleep(rnd.Next(1100, 1338));
							}
						}

						Thread.Sleep(rnd.Next(200, 238));
						input.SendKey(Keys.F1);
						Thread.Sleep(rnd.Next(200, 238));

						input.MoveMouseTo(match["+" + currentStep.ToString()].X, match["+" + currentStep.ToString()].Y);

						Thread.Sleep(rnd.Next(10, 38));
						input.SendMouseEvent(MouseState.LeftDown);
						Thread.Sleep(rnd.Next(120, 158));

						input.MoveMouseTo(match["PutItem"].X, match["PutItem"].Y);
						Thread.Sleep(rnd.Next(120, 158));
						input.SendMouseEvent(MouseState.LeftUp);
						Thread.Sleep(rnd.Next(10, 38));
						Thread.Sleep(rnd.Next(120, 158));

						if (!botThreadStop)
						{
							SetForegroundWindow(Handle);
							return 0;
						}

						input.MoveMouseTo(match["btnStart"].X, match["btnStart"].Y);
						Thread.Sleep(rnd.Next(70, 108));
						input.SendLeftClick();
						Thread.Sleep(rnd.Next(70, 108));
						input.SendLeftClick();

						if (currentStep >= 3)
						{
							if (!botThreadStop)
							{
								SetForegroundWindow(Handle);
								return 0;
							}

							if (!match.ContainsKey("btnSuccess"))
							{
								if (!AddMatch(new string[] { "btnSuccess" }, GetElement("Window")))
								{
									SetForegroundWindow(Handle);
									return 2;
								}
							}

							input.MoveMouseTo(match["btnSuccess"].X, match["btnSuccess"].Y);
							Thread.Sleep(rnd.Next(70, 108));
							input.SendLeftClick();
						}

						if (!botThreadStop)
						{
							SetForegroundWindow(Handle);
							return 0;
						}

						Thread.Sleep(rnd.Next(2350, 2550));

						if (currentStep < 2)
						{
							for (int i = currentStep; i < 2; i++)
							{
								if (!botThreadStop)
								{
									SetForegroundWindow(Handle);
									return 0;
								}

								input.MoveMouseTo(match["btnStart"].X, match["btnStart"].Y);

								Thread.Sleep(rnd.Next(70, 108));
								input.SendLeftClick();
								Thread.Sleep(rnd.Next(70, 108));
								input.SendLeftClick();
								Thread.Sleep(rnd.Next(70, 108));
								input.SendLeftClick();

								if (!botThreadStop)
								{
									SetForegroundWindow(Handle);
									return 0;
								}

								Thread.Sleep(rnd.Next(2350, 2550));
							}
						}

						if (!botThreadStop)
						{
							SetForegroundWindow(Handle);
							return 0;
						}

						input.MoveMouseTo(match["btnClose"].X, match["btnClose"].Y);
						Thread.Sleep(rnd.Next(70, 108));
						input.SendLeftClick();
						Thread.Sleep(rnd.Next(70, 108));
						input.SendLeftClick();

					}
				}
				input.SendKey(Keys.Escape);
				Thread.Sleep(500);

				if (!botThreadStop)
				{
					SetForegroundWindow(Handle);
					return 1;
				}

				SetForegroundWindow(Handle);

				return 1;
			}
			catch (Exception e)
			{
				int errorCode = Marshal.GetLastWin32Error();
				MessageBox.Show($"errorCode: {errorCode}, StackTrace: {e.StackTrace}");
				throw new Win32Exception(errorCode, e.StackTrace);
			}
		}

	}
}
